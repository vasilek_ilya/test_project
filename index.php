<?php

spl_autoload_register(function ($class) {
    include $class . '.php';
});

define('ROOT', __DIR__);

include_once('Helpers\FrontController.php');
\Helpers\FrontController::run();
