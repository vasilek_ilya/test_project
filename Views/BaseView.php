<?php

namespace Views;


class BaseView implements View
{
    const VIEW_DIR = ROOT . '/Views/';

    private $view_name;
    private $dir_mane;

    public function __construct($dir_name, $view_name)
    {
        $this->view_name = $view_name;
        $this->dir_mane = $dir_name;
    }

    public function show($data)
    {
        require_once (self::VIEW_DIR . $this->dir_mane . DIRECTORY_SEPARATOR . $this->view_name . '.php');
    }
}