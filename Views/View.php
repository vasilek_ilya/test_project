<?php

namespace Views;

interface View
{
    public function show($data);
}