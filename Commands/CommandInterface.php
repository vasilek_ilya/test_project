<?php

namespace Commands;

use Views\View;

interface CommandInterface
{
    public function execute();
}