<?php

namespace Commands;

use Commands\Context\CommandContext;
use Models\DB\User;
use Views\BaseView;

/**
 * Class for dynamic view
 * getting content by ID
 *
 * Class ViewTemplateCommand
 * @package Commands
 */
class ViewTemplateCommand extends Command implements CommandInterface
{
    private $data;

    /**
     * ViewTemplateCommand constructor.
     * @param CommandContext $context
     */
    public function __construct(CommandContext $context)
    {
        parent::__construct($context);
    }

    /**
     * Show view with user
     *
     * @return bool
     */
    public function execute()
    {
        $dir_name = array_shift($this->command_context->params);
        $view_name = array_shift($this->command_context->params);
        $view = new BaseView($dir_name, $view_name);

        call_user_func_array([$this, 'prepare'], $this->command_context->params);

        $view->show($this->data);

        return true;
    }

    private function prepare($id)
    {
        $this->data['user'] = User::find($id);
    }
}