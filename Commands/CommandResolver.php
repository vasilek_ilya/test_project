<?php

namespace Commands;

use Commands\Context\CommandContext;

class CommandResolver
{
    private $command_context;
    private $command_path;

    public function __construct(CommandContext $context)
    {
        $this->command_context = $context;
        if (file_exists(ROOT . '/config/routes.php')) {
            $this->command_path = include_once (ROOT . '/config/routes.php');
        }
    }

    public function getCommand()
    {
        $uri = $this->command_context->REQUEST_URI;

        foreach ($this->command_path as $reg_uri => $route) {
            if (preg_match("~$reg_uri~", $uri)) {
                $internal_route = preg_replace("~$reg_uri~", $route, $uri);
                $segments = explode('/', $internal_route);

                $command_name = 'Commands\\' . ucfirst(array_shift($segments)) . 'Command';

                $this->command_context->params = $segments;

                if (file_exists(ROOT . '/' . $command_name . '.php')) {
                    include_once (ROOT . DIRECTORY_SEPARATOR . $command_name . '.php');

                    $command_object = new $command_name($this->command_context);
                    return $command_object;
                }
            }
        }

    }
}