<?php

namespace Commands;

use Commands\Context\CommandContext;
use Views\BaseView;
use Views\View;

/**
 * Class for static view
 *
 * Class ViewCommand
 * @package Commands
 */
class ViewCommand extends Command implements CommandInterface
{
    /**
     * ViewCommand constructor.
     * @param CommandContext $context
     */
    public function __construct(CommandContext $context)
    {
        parent::__construct($context);
    }

    /**
     * Show view
     *
     * @param View $view
     * @return bool
     */
    public function execute()
    {
        $dir_name = array_shift($this->command_context->params);
        $view_name = array_shift($this->command_context->params);
        $view = new BaseView($dir_name, $view_name);
        $view->show(false);

        return true;
    }
}