<?php

namespace Commands\Context;

class Request implements CommandContext
{
    private static $instance;
    private $request_params;

    private function __construct()
    {
        foreach ($_SERVER as $key => $arg) {
            $this->request_params[$key] = $arg;
        }
    }

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function all()
    {
        return $this->request_params;
    }

    public function __get($name)
    {
        if (isset($this->request_params[$name])) {
            return $this->request_params[$name];
        }
        return null;
    }
}