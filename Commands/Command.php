<?php

namespace Commands;

use Commands\Context\CommandContext;

abstract class Command
{
    protected $command_context;

    public function __construct(CommandContext $context)
    {
        $this->command_context = $context;
    }
}