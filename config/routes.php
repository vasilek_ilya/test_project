<?php

return [
    '/view/([0-9]+)' => 'viewTemplate/site/view/$1',

    '/contact' => 'view/site/contact',
    '/about' => 'view/site/about',
    '/feedback' => 'view/site/feedback',
    '/' => 'view/site/index',
];