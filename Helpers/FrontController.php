<?php

namespace Helpers;

use Commands\CommandResolver;
use Commands\Context\Request;
use Models\DB\User;

class FrontController
{
    private function __construct()
    {
    }

    public static function run()
    {
        $controller = new self();
        $controller->init();
        $controller->handleRequest();
    }

    private function init()
    {
        //init
    }

    private function handleRequest()
    {
        $request = Request::instance();
        $resolver = new CommandResolver($request);
        $command = $resolver->getCommand();
        $command->execute();
    }
}