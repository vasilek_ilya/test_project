<?php

namespace Models\DB;

use Models\Model;
use PDO;

class User extends Model
{
    protected static $table = 'users';

    protected function __construct($params = null)
    {
        parent::__construct($params);
    }

    public static function find($id)
    {
        $db = self::connect();
        $sql = 'SELECT * FROM users WHERE id = :id;';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return new self($result->fetch());
    }

    public static function instance()
    {
        return new self();
    }

    public function save()
    {
        if (isset($this->params['id'])) {
            echo 'update user';
        } else {
            echo 'create user';
        }
    }
}