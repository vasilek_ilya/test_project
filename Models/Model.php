<?php

namespace Models;

use PDO;

abstract class Model
{
    protected $params = [];
    protected static $table;

    protected function __construct($params = null)
    {
        $this->params = $params;
    }

    protected static function connect()
    {
        $paramPath = ROOT . '/config/db_config.php';
        $params = include($paramPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=utf8";
        $db = new PDO($dsn, $params['user'], $params['password']);

        return $db;
    }

    public function __get($name)
    {
        if (!empty($this->params)) {
            return $this->params[$name];
        }
    }

    public function __set($name, $value)
    {
        if ($name != 'id') {
            $this->params[$name] = $value;
        }
    }
}